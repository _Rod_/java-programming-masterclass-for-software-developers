package com.gmail.ryitlearning;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public final class HeavenlyBody {
    private final String name;
    private final double orbitalPeriod;
    private final Set<HeavenlyBody> satellites;

    public HeavenlyBody(String name, double orbitalPeriod) {
        this.name = name;
        this.orbitalPeriod = orbitalPeriod;
        this.satellites = new HashSet<>();
    }

    public String getName() {
        return name;
    }

    public double getOrbitalPeriod() {
        return orbitalPeriod;
    }

    public boolean addMoon(HeavenlyBody moon) {
        return this.satellites.add(moon);
    }

    public Set<HeavenlyBody> getSatellites() {
        return new HashSet<>(this.satellites);
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (o == null || getClass() != o.getClass()) return false;
//        HeavenlyBody that = (HeavenlyBody) o;
//        return Double.compare(that.getOrbitalPeriod(), getOrbitalPeriod()) == 0 &&
//                getName().equals(that.getName()) &&
//                getSatellites().equals(that.getSatellites());
//    }

    @Override
    public boolean equals(Object body) {
        if (this == body) return true;

        System.out.println("obj.getClass() is " + body.getClass());
        System.out.println("this.getClass() is " + this.getClass());

        if ((body == null) || (body.getClass() != this.getClass())) return false;

        String bodyName = ((HeavenlyBody) body).getName();
        return this.name.equals(bodyName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getOrbitalPeriod(), getSatellites());
    }
}
